#!/bin/bash
# Copyright Lukas Fend 2016
# Colors
BLACK=$(tput setaf 0)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
LIME_YELLOW=$(tput setaf 190)
POWDER_BLUE=$(tput setaf 153)
BLUE=$(tput setaf 4)
MAGENTA=$(tput setaf 5)
CYAN=$(tput setaf 6)
WHITE=$(tput setaf 7)
BRIGHT=$(tput bold)
NORMAL=$(tput sgr0)
BLINK=$(tput blink)
REVERSE=$(tput smso)
UNDERLINE=$(tput smul)
# Colors: Thanks to http://stackoverflow.com/a/4332530
printf "${BRIGHT}${CYAN}DebianUpgraded by (c) Lukas Fend${NORMAL}\n"

# Functions
function confirmAdmin {
printf "${WHITE}${BRIGHT}"
read -p "Are you in your home directory? (Y/n)" -n 1 -r
printf "\n${GREEN}To get into your home directory type 'cd /home/YOURNAME' ${NORMAL}\n"
if [[ $REPLY =~ ^[Nn]$ ]]
then
	echo "Exiting..."
    exit
fi
}
function installBashTheme {
	printf "${BRIGHT}${WHITE}"
	read -p "Do you want to install the parrot bash theme?(Y/n) " -n 1 -r
	if [[ $REPLY =~ ^[Yy]$ ]]
	then
		printf "\ņ${NORMAL}${LIME_YELLOW}Installing parrot bash theme into .bashrc...${NORMAL}\n"
wget -O .tempBashrc https://gist.githubusercontent.com/rickdaalhuizen90/d1df7f6042494b982db559efc01d9557/raw/488d28c1b614617025b6dc9d8da1075eedb892d4/.bashrc
cat .tempBashrc >> .bashrc
rm .tempBashrc
		printf "\n${LIME_GREEN}Bash theme installed, restart the terminal to see changes.${NORMAL}\n"
	fi
}
function installWallpaper {
	printf "\n${BRIGHT}${WHITE}"
	read -p "Do you want to download the grey parrot wallpaper and enable it? (Y/n)" -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
    	printf "${NORMAL}\n"
	wget -O ~/Pictures/parrotGrey.jpg https://raw.githubusercontent.com/ParrotSec/parrot-wallpapers/master/backgrounds/parrot-fly-grey.jpg

	sudo gsettings set org.gnome.desktop.background picture-uri "file:///home/${USER}/Pictures/parrotGrey.jpg"
fi

        printf "\n${BRIGHT}${WHITE}"
        read -p "Do you want to download the colored parrot wallpaper and enable it? (Y/n)" -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
        printf "${NORMAL}\n"
        wget -O ~/Pictures/parrotAbstract.jpg https://raw.githubusercontent.com/ParrotSec/parrot-wallpapers/master/backgrounds/parrot-abstract.jpg
        gsettings set org.gnome.desktop.background picture-uri "file:///home/${USER}/Pictures/parrotAbstract.jpg"
fi
printf "\n${BRIGHT}${GREEN}Installation complete!${NORMAL}"
}
function customisation {
	printf "\n${BRIGHT}${WHITE}Do you want to continue to the customisation part?"
	read -p "(Y/n) " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
    printf "\n"
	installBashTheme
	installWallpaper
fi
}
function setupPPA {
	printf "${GREEN}In order to install all the packages new PPAs need to be added.\n${BRIGHT}${WHITE}"
	read -p "Do you agree? (Y/n)" -n 1 -r
	if [[ $REPLY =~ ^[Yy]$ ]]
	then
	    	printf "${NORMAL}${LIME_YELLOW}Adding PPAs...${NORMAL}\n"
		printf "${WHITE}Adding Spotify PPA...\n${NORMAL}"
		sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys BBEBDCB318AD50EC6865090613B00F1FD2C19886
		echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list
		printf "${WHITE}Adding mono PPA...\n${NORMAL}"
		sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
		echo "deb http://download.mono-project.com/repo/debian wheezy main" | sudo tee /etc/apt/sources.list.d/mono-xamarin.list
		sudo add-apt-repository ppa:noobslab/macbuntu -y
		sudo add-apt-repository ppa:noobslab/themes -y
		printf "${WHITE}All PPA's added! Updating...\n${NORMAL}"
		sudo apt update -y
		printf "${WHITE}PPA's added and updated successfully!\nStarting with package installation...\n${BRIGHT}${WHITE}"
		read -p "This will download and install quite a lot of data, do you want to automatically agree to install every single package?(Y/n) " -n 1 -r
		if [[ $REPLY =~ ^[Yy]$ ]]
			then
			   	PREFIX="sudo apt install -y "
				SUFFIX=""
			fi
			else 
				PREFIX="sudo apt install "
				SUFFIX=""
		fi	
		printf "${NORMAL}${WHITE}"
		packages
		printf "${BRIGHT}${LIME_YELLOW}All packages installed!${NORMAL}"
		customisation
}

function packages {
	${PREFIX}cowsay${SUFFIX}
	${PREFIX}git${SUFFIX}
	${PREFIX}nodejs${SUFFIX}
	${PREFIX}npm${SUFFIX}
	${PREFIX}brasero${SUFFIX}
	${PREFIX}vim${SUFFIX}
	${PREFIX}htop${SUFFIX}
	${PREFIX}ntop${SUFFIX}
	${PREFIX}fortune${SUFFIX}
	${PREFIX}wine${SUFFIX}
	${PREFIX}virtualbox${SUFFIX}
	${PREFIX}unity-tweak-tool${SUFFIX}
	${PREFIX}openjdk-8-jre${SUFFIX}
	${PREFIX}vlc${SUFFIX}
	${PREFIX}qbittorrent${SUFFIX}
	${PREFIX}python${SUFFIX}
	${PREFIX}gparted${SUFFIX}
	${PREFIX}spotify${SUFFIX}
	${PREFIX}mono${SUFFIX}
	${PREFIX}yum${SUFFIX}
	${PREFIX}macbuntu-os-icons-lts-v8${SUFFIX}
	${PREFIX}macbuntu-os-ithemes-lts-v8${SUFFIX}
	${PREFIX}macbuntu-os-plank-theme-lts-v8${SUFFIX}
	${PREFIX}macbuntu-os-lightdm-lts-v8${SUFFIX}
}


OPTION=$1
PREFIX=""
SUFFIX=""
case $OPTION in
	i*)
		confirmAdmin
		setupPPA
	;;
	u*)
		confirmAdmin
	;;
	p*)
		confirmAdmin
	;;
	h*)
		printf "${LIME_YELLOW}List of options:${NORMAL}\n"
		printf "${GREEN}Argument\tShort\tDescription\n"
		printf "${WHITE}"
			printf "install\t\ti\tInstalls all the packages\n"i
			printf "uninstall\tu\tUninstalls all the packages\n"
			printf "purge\t\tp\tUninstalls/Purges all the packages (Including config files!)\n"
			printf "list\t\tl\tLists all packages that will be installed.\n"
			printf "help\t\th\tShows help (this command)\n"
		printf "${NORMAL}"
	;;
	l*)
		PREFIX="printf "		
		SUFFIX="\n"
		printf "${GREEN}${BRIGHT}List of all packages${NORMAL}\n"
		packages
	;;
	*)
		printf "${RED}${BRIGHT}Error: ${NORMAL}${LIME_YELLOW}Invalid argument. (Use: {install | uninstall | purge | list | help}\n"
	;;
esac

